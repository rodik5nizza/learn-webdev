"use strict"

let firstNumber = checkNumber('Enter first number');
let secondNumber = checkNumber('Enter second number')

function checkNumber(massage) {
    let number;

    do  { number = prompt(massage, 5)
    } while (isNaN(+number) || number === '' || number === null);
    
    return +number;
};


let operator = prompt('Enter symbol +, -, *, /.', '+')


function calcNumber (firstNum, secondNum, operation) {
    switch (operation) {
        case "+":
            return firstNum + secondNum;
        case "-":
            return firstNum - secondNum;
        case "*":
            return firstNum * secondNum;
        case "/":
            return firstNum / secondNum;    
    }
};


console.log(calcNumber(firstNumber, secondNumber, operator))