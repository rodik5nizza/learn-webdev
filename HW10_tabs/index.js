"use strict"


const tabs = document.querySelector('.tabs')
const tabsText = document.querySelectorAll('.tabs-text');
const tabsTitle = document.querySelectorAll('.tabs-title');

tabs.addEventListener('click',(e) => {
    let targetItem = e.target;
    let targetItemAttribute = targetItem.getAttribute('data-tabs');
    
    tabsTitle.forEach((tabsTitle) => {
        tabsTitle.classList.remove('active')
    })
    targetItem.classList.add('active')

    tabsText.forEach((tabsText) => {
        tabsText.classList.remove('tabs-content-text')
        if (tabsText.classList.contains(targetItemAttribute)) {
            tabsText.classList.add('tabs-content-text')
        }
    })
})






//old code

// let tab = function () {
//     const list = document.querySelectorAll('.tabs-title')
//     const text = document.querySelectorAll('.tabs-content li')
//     let tabName;

//     list.forEach((elem) => {
//         elem.addEventListener('click', selectList)
//     })

//     function selectList () {
//         list.forEach((elem) => {
//             elem.classList.remove('active')
//         });
//         this.classList.add('active')
//         tabName = this.getAttribute('data-tabs')
//         selectTabContent(tabName);
//     }
//     function selectTabContent (tabName){
//         text.forEach((elem) => {
//             elem.classList.contains(tabName)? elem.classList.add('tabs-content-text') : elem.classList.remove('tabs-content-text')
//         })
//     }
// }


// tab()


