"use strict"

let num;
do {
    num = +prompt('Enter some number?');
} while (Number.isInteger(num) == false);

if (num < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 0; i <= num; i++) {
        if (i % 5 === 0) {
            console.log(i);
        };
    };
};  



// for (let i = 1; i <= num; i++) {
//     if (i % 5 === 0) {
//         console.log(i);
//     }
// }

// if (num < 5) {
//     console.log("Sorry, no numbers");
// }
