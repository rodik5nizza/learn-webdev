"use strict"
const body = document.body
const navMenu = document.querySelector('.header__list')
const button = document.querySelector('.header__button');
const navMenuLink = document.querySelectorAll('.header__list-link')

const srcSecond = './dist/img/icon/menu-button-2.png'
const srcFirst = './dist/img/icon/menu-button-1.png'

button.addEventListener('click', () => {
    let buttonSrc = button.getAttribute('src');
    if (buttonSrc !== srcSecond) {
        button.setAttribute('src', srcSecond);
        navMenu.classList.toggle('header__list--disable')
    } else {
        cansrlMenu();
    };
});

navMenu.addEventListener('click', (e) => {
    if (e.target != e.currentTarget) {
        navMenuLink.forEach((elem) => {
            elem.classList.remove('header__list-link--active');
            e.target.classList.add('header__list-link--active');
        });
        cansrlMenu();
    };
});

body.addEventListener('click', (e) => {
    if (e.target !== button) {
        cansrlMenu();
    };
});

function cansrlMenu() {
    navMenu.classList.add('header__list--disable');
    button.setAttribute('src', srcFirst);
};