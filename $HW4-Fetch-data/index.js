"use strict"

const url = 'https://ajax.test-danit.com/api/swapi/films';

fetch(url).then(response => response.json())
.then((result) => {
    result.forEach(({characters, episodeId, name, openingCrawl}) => {
        document.querySelector(`.episod-${episodeId}`).insertAdjacentHTML('afterbegin', `<h1>Star Wars Episod ${episodeId} "${name}"</h1>`)
        document.querySelector(`.episod-${episodeId}`).insertAdjacentHTML('beforeend', `<h2>${openingCrawl}</h2>`)
        characters.forEach((url) => {
            fetch(url).then(response => response.json())
            .then(({name: firstName}) => {
                document.querySelector(`.list-${episodeId}`).insertAdjacentHTML('afterbegin',`<li>${firstName}</li>`)
            })
        })
    })   
})
