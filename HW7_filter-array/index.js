const arr = ['hello', 23, '23', null, true, false];

let filterBy = (array, type) => array.filter(elem => typeof elem !== type);



const nonStrings = filterBy(arr, 'string');
console.log(nonStrings);

const nonNumbers = filterBy(arr, 'number');
console.log(nonNumbers);


const nonBooleans = filterBy(arr, 'boolean');
console.log(nonBooleans);


const nonObjects = filterBy(arr, 'object');
console.log(nonObjects);


