"use strict"

const buttonTheme = document.querySelector('.button-theme');
let themeAttribute = document.documentElement;

buttonTheme.addEventListener('click', () => {
    if (themeAttribute.hasAttribute('data-theme')) {
        themeAttribute.removeAttribute('data-theme');
        localStorage.removeItem('theme');
    } else {
        themeAttribute.setAttribute('data-theme', 'dark');
        localStorage.setItem('theme', 'dark');
    }
});

if (localStorage.getItem('theme') !== null) {
    themeAttribute.setAttribute('data-theme', 'dark');
};