"use strict"

const imageCollection = document.querySelectorAll('img');

let cycleImage = setInterval(ImageSlider, 3000);
let indexImage = 1;

function ImageSlider() {
   if (indexImage == imageCollection.length){
        imageCollection[indexImage - 1].classList.remove('image')
        indexImage = 0;  
    } else {
        imageCollection[indexImage - 1].classList.remove('image')
    };
    imageCollection[indexImage].classList.add('image');
    indexImage += 1;
};

const stopButton = document.querySelector('.stop');
stopButton.addEventListener('click', () => clearInterval(cycleImage));


const refreshButton = document.querySelector('.refresh');
refreshButton.addEventListener('click', () => {
    clearInterval(cycleImage);
    cycleImage = setInterval(ImageSlider, 3000)
});
