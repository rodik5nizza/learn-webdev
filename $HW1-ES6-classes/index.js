"use strict"

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return `Name: ${this._name}`;
    }
    set name(newName) {
        this._name = newName;
    }
    get age() {
        return `Age: ${this._age}`;
    }
    set age(newAge) {
        this._age = newAge;
    }
    get salary() {
        return `Salary: ${this._salary}`;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }
    get salary() {
        return `Salary: ${this._salary * 3}`;
    }
}

const programmer = new Programmer('Nill', 21, 2000, 'C++')
const frontEnd = new Programmer('Rob', 25, 1000, 'JS')
const backEnd = new Programmer('Roger', 27, 2500, 'Java')

console.log(programmer);
console.log(frontEnd);
console.log(backEnd);