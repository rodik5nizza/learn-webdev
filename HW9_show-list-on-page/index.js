"use strict"

const cityArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];;
const  searchList = document.querySelector('ul');

const arrayList = (array, parent = document.body) => {
const arrayMap = array.map((elem) => {
   return `<li>${elem}</li>`
});

parent.insertAdjacentHTML('afterbegin', arrayMap.join(''));
};

arrayList(cityArray, searchList);
