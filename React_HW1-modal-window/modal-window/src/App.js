import React from 'react';
import styles from './App.module.scss'
import Header from './components/Header/Header';
import CardWrapper from './components/CardWrapper/CardWrapper';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';


class App extends React.Component {
  state = {
    stateModal: false,
    data: [],
    carts: [],
    value: 0,
  }

  async componentDidMount() {
    const data = await fetch('./data.json').then(res => res.json())
    this.setState({ data })

    const datas = localStorage.getItem('data')
    const cart = localStorage.getItem('cart')
    if (datas) {
      this.setState({ data: JSON.parse(datas)})
    }
    if (cart) {
      this.setState({ carts: JSON.parse(cart)})
    }
  }

  handleClick = (e, value) => {
    if (e.target === e.currentTarget) {
      this.setState((state) => {
        return { stateModal: !state.stateModal, value: value }
      })
    };
  }

  inFavorites = (value) => {
    this.setState((current) => {
      const data = [...current.data];
      const index = data.findIndex((elem) => elem.article === value)

      if (index !== -1) {
        if (!data[index].counter) {
          data[index].counter = 1;
        } else {
          delete data[index].counter
        }
        data[index].isFavourite = !data[index].isFavourite
      }
      localStorage.setItem('data', JSON.stringify(data))
      return { data }

    })
  }


  addToCart = () => {
    this.setState((current) => {
      const cart = [...current.carts]     
        const index = cart.findIndex((el) => el.article === this.state.value.article)
        index === -1 ? cart.push({...this.state.value, counter : 1}) : cart[index].counter += 1 

      localStorage.setItem('cart', JSON.stringify(cart))
      return { carts : cart,  stateModal: !current.stateModal}

    })
  }

  counter(data) {
    if(data) {
      let count = 0;
      data.forEach(({ counter }) => {
        if (counter) {
          count += counter
        }
      })
      return count
    }
  }

  render() {
    return (
      <div className={styles.App}>
        <Header counter={this.counter(this.state.data)} counterCart={this.counter(this.state.carts)} />
        <h2 className={styles.title}>Список товаров</h2>
        <CardWrapper heandler={this.inFavorites} data={this.state.data} handleClick={this.handleClick} />
        {this.state.stateModal && <Modal action={<><Button handleClick={this.addToCart} text='OK' backgroundColor='red' /> <Button handleClick={this.handleClick} text='Close' backgroundColor='#D44637' /></>} header='Хотите добавить в корзину?' text="Если вы хотите добавить товар в корзину, нажмите ОК!" closeButton={true} handleClick={this.handleClick} />}
      </div>
    );
  }

}

export default App;
