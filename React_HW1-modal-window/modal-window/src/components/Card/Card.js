import React from "react";
import Button from "../Button/Button";
import styles from "./Card.module.scss";
import starActive from "./starActive.svg";
import star from './star.svg';
import PropTypes from "prop-types";

export default class Card extends React.PureComponent {
    render() {
        const { article, color, isFavourite, name, path, price, heandler, handleClick } = this.props;
        return (
            <div className={styles.card}>
                <div className={styles.cardContainer}>
                    <div onClick={()=> heandler(this.props.article)} className={styles.star}>
                    {isFavourite && <img src={starActive} alt="starActive"/>}
                        {!isFavourite && <img src={star} alt="star"/>}
                    </div>
                    <div className={styles.cardImg}>
                        <img src={path} alt={name} />
                    </div>
                    <p className={styles.cardTitle}>{name} {color}</p>
                    <p className={styles.cardPrice}>{price} UAH</p>
                    <Button handleClick={(e)=> handleClick(e, {article, color, isFavourite, name, path, price })} text="Add To Cart" backgroundColor="gray" />
                </div>
            </div>
        )
    }
}
Card.propTypes = {
    article: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    isFavourite: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
    path: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    heandler: PropTypes.func,
    handleClick: PropTypes.func,
}

Card.defaultProps = {
    heandler: ()=>{},
    handleClick: ()=> {},
}