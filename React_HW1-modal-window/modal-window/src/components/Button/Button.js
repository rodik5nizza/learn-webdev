import React from "react";
import styles from './Button.module.scss'
import PropTypes from "prop-types";

export default class Button extends React.PureComponent{
    
    render(){
        
        const { backgroundColor, text, handleClick, modalNumber } = this.props
        return (
            <button className={styles.btn} style={ {backgroundColor:backgroundColor} } onClick={ (e) => handleClick(e, modalNumber) }>{ text }</button>
        )
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    handleClick: PropTypes.func,
    modalNumber: PropTypes.number,
}

Button.defaultProps = {
    hendleClick: () => {},
    modalNumber: 1,
}