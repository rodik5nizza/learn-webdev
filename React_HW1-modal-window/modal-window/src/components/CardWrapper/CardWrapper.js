import React from "react";
import Card from "../Card/Card";
import styles from "./CardWrapper.module.scss";
import PropTypes from "prop-types";

export default class CardWrapper extends React.PureComponent {
    render() {
        const { data, heandler, handleClick } = this.props
        return (
            <ul className={styles.CardWrapper}>
                {data.map(({ article, color, isFavourite, name, path, price }) => {
                    return (
                        <li className={styles.CardItem} key={article}>
                            <Card handleClick={handleClick} article={article} heandler={heandler} color={color} isFavourite={isFavourite} name={name} path={path} price={price} />
                        </li>
                    )
                })
                }
            </ul>
        )
    }
}

CardWrapper.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        article: PropTypes.number,
        color: PropTypes.string,
        isFavourite: PropTypes.bool,
        name: PropTypes.string,
        path: PropTypes.string,
        price: PropTypes.number,
      })).isRequired,
    heandler: PropTypes.func, 
    handleClick: PropTypes.func,
}

CardWrapper.defaultProps = {
    heandler: () => {}, 
    handleClick: () => {},
}