import React from "react";
import styles from './Modal.module.scss';
import PropTypes from "prop-types";

export default class Modal extends React.Component {
    render() {
        const { action, header, text, closeButton, handleClick } = this.props;

        return (
            <div className={styles.background} onClick={(e) => handleClick(e)}>
                <div className={styles.modal}>
                    <div className={styles.headerWrapper}>
                    <p className={styles.modalHeader}>{header}</p>
                    {closeButton && <span onClick={(e) => handleClick(e)} className={styles.modalClose}>X</span>}
                    </div> 
                    <p className={styles.modalText}>{text}</p>
                    {action}
                </div>
            </div>

        )
    }
}

Modal.propTypes = {
    action: PropTypes.node.isRequired, 
    header: PropTypes.string.isRequired, 
    text: PropTypes.string.isRequired, 
    closeButton: PropTypes.bool, 
    handleClick: PropTypes.func,
}

Modal.defaultProps = {
    closeButton: true, 
    handleClick: ()=>{},
}