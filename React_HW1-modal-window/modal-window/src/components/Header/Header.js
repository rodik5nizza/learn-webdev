import React from "react";
import styles from './Header.module.scss';
import cart from './cart.svg';
import logo from './logo.svg';
import star from './starActive.svg';
import PropTypes from "prop-types";

export default class Header extends React.Component {
    render() {
        const { counter, counterCart } = this.props
        return (
            <div className={styles.header}>
                <div className={styles.logoWrapper}>
                    <img src={logo} alt="logo" width={50} />
                    <h1>ChairShop</h1>
                </div>
                <div className={styles.cartWrapper}>
                    <div className={styles.starWrapper}>
                        <img className={styles.star} src={star} alt="star" width={40} />
                        {counter > 0 && <div className={styles.counter}>{counter}</div>}
                    </div>
                    <div className={styles.cart}>
                        <img className={styles.cart} src={cart} alt="cart" width={40} />
                        {counterCart > 0 && <div className={styles.counter}>{counterCart}</div>}
                    </div>
                </div>
            </div>
        )
    }
}
Header.propTypes = {
    counter: PropTypes.number, 
    counterCart: PropTypes.number,
}

Header.defaultProps = {
    counter: 0, 
    counterCart: 0,
}