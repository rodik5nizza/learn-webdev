"use strict"

const form = document.querySelector('form')
const btn = document.querySelectorAll('.fas')

btn.forEach(btn => {
    btn.addEventListener('click', (e) => {
        let target = e.target.getAttribute('data-a');
        let inputPass = document.querySelector(target);

        if (inputPass.getAttribute('type') === 'password') {
            inputPass.setAttribute('type', 'text');
            btn.classList.toggle('fa-eye');
            btn.classList.toggle('fa-eye-slash');
        } else {
            inputPass.setAttribute('type', 'password')
            btn.classList.toggle('fa-eye');
            btn.classList.toggle('fa-eye-slash');
        }
    })
})

const error = document.querySelector('p')
const pass = document.querySelector('#passwordFirst')
const confirmPass = document.querySelector('#passwordSecond')

form.addEventListener('submit', () => {
    if (pass.value == confirmPass.value) {
        alert('You are welcome')
    } else {
        error.style.display = 'inline'
    }
})

