const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class NotDataError extends Error {
    constructor(errorName) {
        super();
        this.name = 'NotDataError';
        this.message = `Invalid format book ${errorName}`; 
    }
}

class BookStorage {
    constructor(author, name, price) {
        if (author === undefined) {
            throw new NotDataError('AUTHOR')
        } else if (name === undefined){
            throw new NotDataError('NAME')
        } else if (price === undefined){
            throw new NotDataError('PRICE')
        };
        this.author = author;
        this.name = name;
        this.price = price;
    };
    render(container) {
        const root = document.querySelector('#root');
        root.append(container);
        container.insertAdjacentHTML('beforeend', `<li><div>Autor: ${this.author}</div><div>Name: ${this.name}</div><div>Price: ${this.price}</div></li>`);
    };
};

const container = document.createElement('ul');

books.forEach((elem) => {
    try {
        new BookStorage(elem.author, elem.name, elem.price).render(container);
    }
    catch (error) {
        if (error.name === 'NotDataError') {
            console.warn(error);
        } else {
            throw error;
        }
    }

})