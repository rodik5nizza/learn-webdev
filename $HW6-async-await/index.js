"use strict"

const btn = document.querySelector('button');

btn.addEventListener('click', findIP)

async function findIP(){
    const {ip} = await fetch('https://api.ipify.org/?format=json').then(response => response.json())
    console.log(ip)
    
    const {timezone, country, countryCode, city, regionName} = await fetch(`http://ip-api.com/json/${ip}`).then(resp => resp.json())

    btn.insertAdjacentHTML('afterend', `<div    >Временная зона : ${timezone}</div>
    <div>Страна : ${country}</div>
    <div>Код страны : ${countryCode}</div>
    <div>Регион : ${city}</div>
    <div>Город : ${regionName}</div>`)
    
}
