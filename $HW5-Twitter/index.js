"use strict"

const urlUser = 'https://ajax.test-danit.com/api/json/users'
const urlPosts = 'https://ajax.test-danit.com/api/json/posts'

class Card {
    constructor(name, email, title, body, id) {
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
        this.id = id;
    }

    render() {
        const card = document.createElement('div')
        card.className = 'card';
        const btn = document.createElement('a')
        btn.href = '#'
        card.append(btn)
        card.insertAdjacentHTML('beforeend', `<div class="card-header">
        <h3>${this.name}</h3><span>${this.email}</span> 
        </div>
        <div class="card-body">
        <h2>${this.title}</h2>
        <p>${this.body}</p>
        </div>`)

        document.body.append(card)

        btn.addEventListener('click', (e) => {
            e.preventDefault()
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method: 'DELETE',
            })
                .then(({ ok }) => {
                    if (ok) {
                        btn.closest('.card').remove()
                    }
                })
        })
    }

}

const card = () => {
    fetch(urlUser).then(response => response.json())
        .then(result => {
            fetch(urlPosts).then(response => response.json())
                .then(res => {
                    result.forEach(({ name, email, id }) => {
                        res.forEach(({ title, body, userId, id: postID }) => {
                            if (id === userId) {
                                new Card(name, email, title, body, postID).render()
                            }
                        })
                    })
                })

        })
}
// old version
// const card = () => {
//     fetch(urlPosts).then(response => response.json())
//         .then(result => {
//             result.forEach(({ title, body, userId, id: postID }) => {
//                 fetch(urlUser).then(response => response.json())
//                     .then(res => {
//                         res.forEach(({ name, email, id }) => {
//                             if (userId === id) {
//                                 new Card(name, email, title, body, postID).render()
//                             }
//                         })
//                     })
//             })
//         })
// }


card()


